Demo koajs backend implementing the following API:
  - authorization (several predetermined users);
  - data request authentication (getCounter, setCounter).
The backend implements a specific function of the increment (n0 = 0, n1 = 1, n2 = n1 * 2, ni = ni-1 * 2).


## Prerequisites

* Node.js (8+): recommend using [nvm](https://github.com/creationix/nvm)

## Development

During development, the `/app` folder is being watched for changes.

All changes invoke the TypeScript compiler, which restarts the app upon completion.

```shell
npm run watch
```

## Build the Server

To compile the TypeScript code and place into the `/dist` folder:

```shell
npm build
```

## Start local server

```shell
npm start
```

## Code Linter

A TypeScript linter has been added to keep code consistent among developers.

```shell
npm run lint
```

To autofix linting errors (not all errors are auto-fixable):

```shell
npm run fix
```
