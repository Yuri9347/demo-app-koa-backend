import * as Koa from 'koa';
import * as koaBody from 'koa-body';
import * as cors from '@koa/cors';

const koaValidator = require('koa-async-validator');
const koaBunyanLogger = require('koa-bunyan-logger');

import { config } from './config';
import { routes, allowedMethods } from './routes';
import { logger } from './logger';
import { auth } from './auth_local';

const app = new Koa();

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    if (err.status === 401) {
      ctx.status = 401;
      ctx.body = {
        error: err.originalError ? err.originalError.message : err.message
      };
    } else if (err.status === 400) {
      ctx.status = err.status;
      ctx.body = {
        error: {
          code: ctx.status,
          message: err.message
        }
      };
    } else if (process.env.WORK_MODE != 'prod') {
        ctx.status = err.status || 500;
        ctx.body = {
          error: {
            code: ctx.status,
            message: err.message
          }
        };
    } else {
      throw err;
    }
  } // catch (err) {
});

app.use(koaBody());
app.use(koaValidator());
app.use(cors());
app.use(koaBunyanLogger(logger));
app.use(koaBunyanLogger.requestLogger());
app.use(koaBunyanLogger.timeContext());
app.use(auth.initialize());
app.use(routes);
app.use(allowedMethods);

export const server = app.listen(config.port);

console.log(`Server running on port ${config.port}`);
