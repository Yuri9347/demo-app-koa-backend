import { compare, hashSync } from 'bcrypt';

export interface IUser {
  id: number;
  username: string;
  password: string;
  counter: number;
  comparePass(candidatePassword: string): Promise<boolean>;
}

class User implements IUser {
  password: string;
  username: string;
  counter: number = 0;
  id: number = 0; // TODO The demo example simply sets the id, the value must be unique.

  constructor(password: string, username: string, id: number) {
    this.password = hashSync(password, 10);
    this.username = username;
    this.id = Math.abs(id);
  }

  comparePass(candidatePassword: string): Promise<boolean> {
    const password = this.password;
    return new Promise((resolve, reject) => {
      compare(candidatePassword, password, (err, success) => {
        if (err || !success) return reject(new Error('Bad password'));
        return resolve(success);
      });
    });
  }
}

export class Users {

  private static users: Array<IUser> = [new User('cool_pass', 'admin', 1),
    new User('123456', 'user', 2)];

  static async getUserByUsername(username: string): Promise<IUser> {
    const lUsers = this.users;
    return new Promise<IUser>(((resolve, reject) => {
      const user = lUsers.find( u => u.username === username);
      if (!user) { reject(new Error('User not found')); }
      else resolve(user);
    }));
  }

}
