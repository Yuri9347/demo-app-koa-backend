import * as passport from 'koa-passport';
import { IStrategyOptions, Strategy } from 'passport-local';
import { Strategy as StrategyJWT, StrategyOptions, ExtractJwt } from 'passport-jwt';
import { Users, IUser } from './users';
import { JWT_SECRET } from './jwt_secret';

class Auth {

  public initialize = () => {
    passport.use('local', this.getLocalStrategy());
    passport.use('jwt', this.getJWTStrategy());
    return passport.initialize();
  };

  // public authenticate = (callback: (...args: any[]) => any) => passport.authenticate('local', { session: false, failWithError: true }, callback);

/*
  public login = async (req, res) => {
    try {
      req.checkBody('username', 'Invalid username').notEmpty();
      req.checkBody('password', 'Invalid password').notEmpty();

      const errors = req.validationErrors();
      if (errors) throw errors;

      const user = await User.findOne({ 'username': req.body.username }).exec();

      if (user === null) throw 'User not found';

      const success = await user.comparePassword(req.body.password);
      if (success === false) throw '';

      res.status(200).json(this.genToken(user));
    } catch (err) {
      res.status(401).json({ 'message': 'Invalid credentials', 'errors': err });
    }
  }
*/

  private getUser = async (name: string): Promise<IUser> => {
    try {
      return await Users.getUserByUsername(name);
    } catch (err) {
      throw err;
    }
  };

  private getLocalStrategy = (): Strategy => {
    const params: IStrategyOptions = {
      usernameField: 'username',
      passwordField: 'password',
      session: false
    };

    return new Strategy(params, (aUsername, aPassword, done) => {

      this.getUser(aUsername)
        .then(user => {
          user.comparePass(aPassword)
            .then( res => done(null, user))
            .catch( err => done(null, false, err.message));
        })
        .catch(err => done(null, false, err.message));
    });
  };

  private getJWTStrategy = (): StrategyJWT => {
    const params: StrategyOptions = {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: JWT_SECRET
    };

    return new StrategyJWT(params, (jwtToken, done) => {
      this.getUser(jwtToken.username)
        .then(user => done(undefined, user))
        .catch(err => done(err.message, false));
    });
  }
}

export const auth = new Auth();
