import * as Router from 'koa-router';
import * as passport from 'koa-passport';
import { sign } from 'jsonwebtoken';

import { JWT_SECRET } from './jwt_secret';
import { loginSchema } from './valid_schemes';

const util = require('util');

const router = new Router();

/**
 * Login
 */
router.post('/login', async (ctx, next) => {
  ctx.checkBody(loginSchema);

  const errors = await ctx.validationErrors();

  if (errors) {
    ctx.body = {
      error: {
        code: 400,
        message: 'Bad Request',
        data: errors
      }
    };
    ctx.status = 400;
  } else {
    await passport.authenticate('local', (unused, user, err) => {
      if ((user == false) || (err != null)) {
        ctx.body = { error: {
            code: 401,
            message: 'User authorization failed.',
            data: err
          } };
      } else {
        const token = sign({username: user.username, user_id: user.id},
          JWT_SECRET,
          {
            expiresIn: '5m',
          });
        ctx.body = { result: {user: user.username, token: token}};
      }
    })(ctx, next);
  }
});

/**
 * getCounter
 */
router.get('/getCounter', async (ctx, next) => {
  await passport.authenticate('jwt', (unused, user, err) => {
    if (user == false) {
      ctx.body = { error: {
          code: 401,
          message: 'User unauthorized.',
          data: `${ util.inspect(err.message) }`
        } };
    } else {
      const tmpVal = user.counter > 1 ? user.counter * 2 : user.counter + 1;
      ctx.body = { result: {user: user.username, counter: user.counter, next_val_counter: tmpVal}};
    }
  })(ctx, next);
});


/**
 * setCounter
 */
router.post('/setCounter', async (ctx, next) => {
  await passport.authenticate('jwt', (unused, user, err) => {
    if (user == false) {
      ctx.body = { error: {
          code: 401,
          message: 'User unauthorized.',
          data: `${ util.inspect(err.message) }`
        } };
    } else {
      user.counter = user.counter > 1 ? user.counter * 2 : user.counter + 1;
      ctx.body = { result: {user: user.username, counter: user.counter, next_val_counter: user.counter > 1 ? user.counter * 2 : user.counter + 1}};
    }
  })(ctx, next);
});

/**
 * Basic healthcheck
 */
router.get('/healthcheck', async ctx => ctx.body = 'OK');

export const routes = router.routes();
export const allowedMethods = router.allowedMethods(({
  throw: false,
  notImplemented: () => { return {
    error: {
      code: 501,
      message: 'Not Implemented'
    }
  }; },
  methodNotAllowed: () => { return {
    error: {
      code: 405,
      message: 'Method Not Allowed'
    }
  }; }
}));
