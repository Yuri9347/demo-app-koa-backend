export const loginSchema = {
  'username': {
    in: 'body',
    notEmpty: true,
    isLength: {
      options: [{ min: 3, max: 32 }],
      errorMessage: 'Must be between 3 and 32 chars long'
    },
    errorMessage: 'Invalid user name'
  },
  'password': {
    in: 'body',
    notEmpty: true,
    isLength: {
      options: [{ min: 6, max: 12 }],
      errorMessage: 'Must be between 6 and 12 chars long'
    },
    errorMessage: 'Invalid Password'
  }
};
